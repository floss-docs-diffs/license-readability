# Quick hack to produce a HTML table of selected readability metrics
# for textfiles. See README.md

if [ ! -f "$1" ]
then
  echo "Usage: $0 LICENSEFILE..."
  exit 1
fi

echo "<table border>"
echo "<tr><th>SHA1</th><th>License</th><th>Characters</th><th>Kincaid</th><th>ARI</th><th>Coleman-Liau</th><th>Fog</th><th>Lix</th><th>SMOG</th><th>Flesch</th><th>Chars/(Flesch>=1)</th></tr>"
for f in $*
do
  LICENSE_NAME=`basename -s .txt "$f"`
  if [ "$LICENSE_NAME" = "README" ]
  then
    continue
  fi
  SHA1=`sha1sum "$f" | cut -d " " -f 1`
  echo -n "<tr><td style='font-size: xx-small'>$SHA1</td><td>$LICENSE_NAME</td><td style='text-align:right'>"
  style "$f" |awk -F '[:/= ]+' '{ OFS="</td><td style='text-align:right'>";
                                  getline;
                                  kincaid=$3;
                                  getline;
                                  ari=$3;
                                  getline;
                                  coleman=$3;
                                  getline;
                                  flesch=$4;
                                  getline;
                                  fog=$4;
                                  getline;
                                  lix=$3;
                                  getline;
                                  smog=$3;
                                  getline;
                                  getline;
                                  chars=$2;
                                  flesch_floor=(flesch>=1)?flesch:1;
                                  pain=int(chars/flesch_floor);
                                  print chars, kincaid, ari, coleman, fog, lix, smog, flesch, pain "</td></tr>";
                                  exit;
                                }'
done
echo "</table>"
